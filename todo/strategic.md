
## TODO's ##

In order of appearance

V means completed
X means can't be done (please provide explanation)

### Bugs:

- do we still need RangeBarResetBus? If it is removed, will there be bugs? *
- what happens when app is killed by the system (Don't keep activity) in different screens? Is there
  a need to save state somewhere?
- check main screen recomposition
- check audio player issues
- issue with "record message" popping up sometimes

 
### Refactoring:

- convert player and recorder to flow
- show snackbar instead of toast
- make record button bigger then others
- add dependency diagram
- main interactor tests
- billing views tests


### Features:

- create scoped track
- check low storage
- zoom in waveform mode V add search
- cover buttons with ui automation
- design local progress and state of track
- add bit counter
- add benchmark tests
- integrate jococo
- add static analysis
- loading screen
- split testing
- website with a blog (similar theme)
- firebase analytics
- firebase feature flags
- add mob 
- analytics
- add developer account profile
- transfer ownership to nl
- add effects
- explore bundles in order to add AI feature as a separate bundle


mvp
- network
- widget
- multy track
- audio effects
- online synchronisation
- most importantly - payment system integration
- collapsable list items
- [native audio player](https://developer.android.com/ndk/guides/audio/aaudio/aaudio)
- google add words
- write downe hepothsis, organize documentation
- add voice recognition AI


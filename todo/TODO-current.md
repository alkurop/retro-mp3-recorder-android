
## TODO's ##

In order of appearance

V means completed
X means can't be done (please provide explanation)

### Bugs:
- seek bar update only on up, send view updates on scroll
 
### Refactoring:

- convert player and recorder to flow
- show snackbar instead of toast
- make record button bigger then others
- add dependency diagram
- main interactor tests
- billing views tests
- player should listen to controls


### Features:
- V loading screen
- repeat
- render speed
- render rewind
- show hide bars with animation
- show hide visualizer when doing speech recognition
- increase record volume
- increase playback volume

#### Speech recognition
- menu
- model links
- download model
- recognition with logs
- recognition list
- save result to db
- timestamps
- crop with timestamps - start, end
- replay with scroll
- add item to list
- copy all

mvp
- audio effects
- google add words
- add voice recognition AI


package com.omar.retromp3recorder.bl.system

import com.omar.retromp3recorder.bl.database.DbUpdaterUCSuspend
import com.omar.retromp3recorder.bl.waveform.WaveformScannerSuspend
import com.omar.retromp3recorder.domain.ExistingFileWrapper
import com.omar.retromp3recorder.domain.isEmpty
import javax.inject.Inject

class WaveformScanUpdaterUC @Inject constructor(
    private val dbUpdaterUC: DbUpdaterUCSuspend,
    private val waveformScanner: WaveformScannerSuspend,
) {
    suspend fun execute(input: List<ExistingFileWrapper>): List<ExistingFileWrapper> {
        val batch = input.filter { it.wavetable.isEmpty() }
        return if (batch.isEmpty()) {
            input
        } else {
            val result = batch.map { existingFileWrapper ->
                waveformScanner.execute(existingFileWrapper)
            }
            dbUpdaterUC.execute(result)
            result
        }
    }
}

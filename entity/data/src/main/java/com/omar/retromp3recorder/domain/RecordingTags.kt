package com.omar.retromp3recorder.domain

data class RecordingTags(
    val title: String,
    val artist: String,
    val year: String
)

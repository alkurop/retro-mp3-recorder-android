package com.omar.retromp3recorder.domain

data class NewNameSuggestion(
    val path: String = "",
    val name: String = ""
)


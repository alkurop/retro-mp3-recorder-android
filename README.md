# Retro MP3 Recorder

https://play.google.com/store/apps/details?id=com.omar.retromp3recorder.app&hl=en&gl=US

Retro MP3 Recorder:

- records audio from mic
- outputs in MP3 format
- user may then share audio file

Develop branch build on CircleCI [![CircleCI](https://dl.circleci.com/status-badge/img/bb/alkurop/retro-mp3-recorder-android/tree/develop.svg?style=svg&circle-token=e7e103db4af4e373bf84282cd50728af360f4550)](https://dl.circleci.com/status-badge/redirect/bb/alkurop/retro-mp3-recorder-android/tree/develop)

package com.omar.retromp3recorder.storage

import androidx.annotation.Keep

@Keep
object SharedPrefsKeys {
    const val AUDIO_SOURCE = "AUDIO_SOURCE"
    const val BIT_RATE = "BIT_RATE"
    const val FILE_NAME = "FILE_NAME"
    const val SAMPLE_RATE = "SAMPLE_RATE"
}

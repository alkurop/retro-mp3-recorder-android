package com.omar.retromp3recorder.storage.db

import androidx.paging.DataSource
import androidx.room.*
import com.omar.retromp3recorder.domain.ExistingFileWrapper
import com.omar.retromp3recorder.domain.Wavetable

@Entity
data class FileDbEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(defaultValue = "0")
    val id: Long,
    val created: Long = 0,
    val lastModified: Long,
    val filepath: String,
    @Embedded
    val waveform: WaveformDbEntity?,
    val length: Long?,
    val name: String?
)

@Entity
data class WaveformDbEntity(
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    val waveform: ByteArray = ByteArray(0),
    val stepMillis: Int? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WaveformDbEntity

        if (!waveform.contentEquals(other.waveform)) return false
        if (stepMillis != other.stepMillis) return false

        return true
    }

    override fun hashCode(): Int {
        var result = waveform.contentHashCode()
        result = 31 * result + (stepMillis ?: 0)
        return result
    }
}

@Dao
interface FileDbEntityDao {

    @Query("SELECT * FROM FileDbEntity ORDER BY id DESC limit 1")
    fun takeLast(): FileDbEntity?

    @Query("SELECT * FROM FileDbEntity ORDER BY lastModified DESC")
    fun getAllPagingData(): DataSource.Factory<Int, FileDbEntity>

    @Query("SELECT * FROM FileDbEntity ORDER BY id DESC limit :pageSize offset :offsetItems")
    fun getAllPaging(pageSize: Int, offsetItems: Int): List<FileDbEntity>

    @Query("SELECT * FROM FileDbEntity where name like :query ORDER BY id DESC limit :pageSize offset :offsetItems COLLATE NOCASE")
    fun getAllPagingQuery(query: String, pageSize: Int, offsetItems: Int): List<FileDbEntity>

    @Query("SELECT * from FileDbEntity WHERE filepath = :filepath")
    fun getByFilepath(filepath: String): List<FileDbEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBatch(items: List<FileDbEntity>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: FileDbEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateItem(item: FileDbEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(items: List<FileDbEntity>)

    @Delete
    suspend fun delete(items: List<FileDbEntity>)

    @Query("DELETE from FileDbEntity where filepath=:filePath")
    suspend fun deleteByFilepath(filePath: String)

    companion object {
        const val LOAD_SIZE = 5
    }
}

fun FileDbEntity.toFileWrapper(): ExistingFileWrapper =
    ExistingFileWrapper(
        this.id,
        this.filepath,
        this.created,
        this.lastModified,
        this.waveform?.toWavetable(),
        this.length
    )

fun ExistingFileWrapper.toDatabaseEntity(): FileDbEntity = FileDbEntity(
    id = this.id,
    created = this.createTimedStamp,
    lastModified = this.modifiedTimestamp,
    filepath = this.path,
    waveform = this.wavetable?.toDatabaseEntity(),
    length = length,
    name = this.name
)

fun Wavetable.toDatabaseEntity() = WaveformDbEntity(this.bytes)
fun WaveformDbEntity.toWavetable() =
    Wavetable(this.waveform)


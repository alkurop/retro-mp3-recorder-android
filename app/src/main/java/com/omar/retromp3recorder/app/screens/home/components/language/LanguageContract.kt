package com.omar.retromp3recorder.app.screens.home.components.language

import androidx.compose.runtime.Immutable
import com.omar.retromp3recorder.domain.RecognitionLanguage

object LanguageContract {
    sealed interface Input {}
    sealed interface Output {
        data class Visibility(val isVisible: Boolean) : Output
    }

    @Immutable
    data class State(
        val isVisible: Boolean = false,
        val selectedLanguage: RecognitionLanguage? = null,
        val availableLanguages: List<RecognitionLanguage> = emptyList()
    )
}

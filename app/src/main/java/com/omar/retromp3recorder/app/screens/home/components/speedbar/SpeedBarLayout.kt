package com.omar.retromp3recorder.app.screens.home.components.speedbar

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.omar.retromp3recorder.app.R
import com.omar.retromp3recorder.app.screens.home.components.Component
import com.omar.retromp3recorder.app.screens.home.components.rememberVisibilityState
import kotlin.math.pow
import kotlin.math.roundToInt

@Composable
fun SpeedBarLayout(
    modifier: Modifier = Modifier,
    viewModel: SpeedBarViewModel = hiltViewModel(),
) {

    val state by viewModel.state.collectAsState()
    val visibilityState = rememberVisibilityState()

    visibilityState.isVisible = state.isVisible
    Component(
        state = visibilityState,
        modifier = modifier
    ) {
        val speedData = state.speedData
        if (speedData != null) {
            var rangeState by remember {
                mutableStateOf(speedData.speed.roundTo(1))
            }
            val active = speedData.isEnabled
            val rangeStateText = stringResource(if (active) R.string.on else R.string.off)
            val rangeText = stringResource(R.string.speed_enabled, "$rangeStateText ${rangeState}x")

            val sendRangeUpdate: (Float) -> Unit = remember {
                {
                    rangeState = it.roundTo(1)
                    viewModel.onEvent(SpeedBarContract.Input.SpeedSet(it))
                }
            }

            val sendEnableEvent: () -> Unit = remember { {
                viewModel.onEvent(SpeedBarContract.Input.Enable)
            } }

            Slider(
                modifier = Modifier.padding(horizontal = 8.dp),
                value = rangeState,
                valueRange = 0.2f..4f,
                onValueChange = sendRangeUpdate,
                steps = 10,
                colors = SliderDefaults.colors(
                    thumbColor = MaterialTheme.colorScheme.tertiary,
                )
            )
            Row(Modifier
                .padding(top = 40.dp)
                .fillMaxWidth()
                .wrapContentHeight()
                .clickable (onClick = sendEnableEvent)
                .padding(horizontal = 8.dp)
                .padding(bottom = 4.dp),
                horizontalArrangement = Arrangement.SpaceBetween) {
                Text(text = stringResource(id = R.string.min_speed))
                Text(text = rangeText)
                Text(text = stringResource(id = R.string.max_speed))
            }
        }
    }
}

fun Float.roundTo(numFractionDigits: Int): Float {
    val factor = 10.0.pow(numFractionDigits)
    return (this * factor).roundToInt() / factor.toFloat()
}

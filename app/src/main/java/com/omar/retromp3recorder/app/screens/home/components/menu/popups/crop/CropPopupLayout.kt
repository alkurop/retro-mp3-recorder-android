package com.omar.retromp3recorder.app.screens.home.components.menu.popups.crop

import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import com.omar.retromp3recorder.app.R
import com.omar.retromp3recorder.app.screens.home.components.menu.popups.common.FileNameContentLayout
import com.omar.retromp3recorder.app.screens.home.components.menu.popups.common.rememberFileNameInputState
import com.omar.retromp3recorder.app.screens.home.components.menu.views.PopupButtonData
import com.omar.retromp3recorder.app.screens.home.components.menu.views.PopupComposable
import com.omar.retromp3recorder.utils.domain.updateName

@Composable
fun CropPopupLayout(
    viewModel: CropViewModelFlow = hiltViewModel(),
    onDismiss: () -> Unit
) {
    val state by viewModel.state.collectAsState()
    val onValueChanged: (String) -> Unit =
        {
            val newNameSuggestion = (state.nameSuggestion to it).updateName()
            viewModel.emit(CropContract.Input.CheckCanCrop(newNameSuggestion))
        }

    if (state.dismiss) {
        SideEffect {
            onDismiss.invoke()
        }
    }

    val nameState = rememberFileNameInputState(name = state.nameSuggestion.name)
    if (state.isVisible) {
        PopupComposable(
            isLoading = state.isLoading,
            title = stringResource(id = R.string.popup_title_crop),
            content = {
                FileNameContentLayout(
                    state = nameState,
                    onValueChanged = onValueChanged,
                    isError = state.isOkEnabled.not()
                )
            },
            onDismiss = onDismiss,
            buttonList = listOf(
                PopupButtonData(isEnabled = state.isOkEnabled,
                    text = stringResource(id = R.string.popup_button_crop_in_place),
                    onClick = { viewModel.emit(CropContract.Input.CropInPlace(state.nameSuggestion)) }),
                PopupButtonData(isEnabled = state.isOkEnabled,
                    text = stringResource(id = R.string.popup_button_crop_outside),
                    onClick = { viewModel.emit(CropContract.Input.CropOutside(state.nameSuggestion)) })
            )
        )
    }
}

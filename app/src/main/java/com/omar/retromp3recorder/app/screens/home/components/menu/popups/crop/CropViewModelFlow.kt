package com.omar.retromp3recorder.app.screens.home.components.menu.popups.crop

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.omar.retromp3recorder.app.screens.home.components.menu.popups.crop.CropMapper.mapToState
import com.omar.retromp3recorder.app.utils.stateInViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CropViewModelFlow @Inject constructor(
    interactor: CropInteractor
) : ViewModel() {

    private val inputFlow = MutableSharedFlow<CropContract.Input>()

    val state = interactor.processIO(inputFlow)
        .mapToState()
        .stateInViewModel(this, CropContract.State())

    fun emit(event: CropContract.Input) {
        viewModelScope.launch { inputFlow.emit(event) }
    }
}

package com.omar.retromp3recorder.app.screens.home.components.menu.views

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.omar.retromp3recorder.app.R
import com.omar.retromp3recorder.app.RetroProgressIndicator

@Composable
fun PopupComposable(
    title: String,
    content: @Composable () -> Unit,
    onDismiss: () -> Unit,
    isLoading: Boolean = false,
    buttonList: List<PopupButtonData> = emptyList(),
    maxHeight:Dp = 170.dp,
    contentPaddingValues: PaddingValues = PaddingValues(16.dp),
    @StringRes dismissButtonRes: Int = R.string.popup_cancel
) {
    Dialog(onDismissRequest = {
        onDismiss.invoke()
    }, content = {
        Surface(
            Modifier
                .fillMaxWidth()
                .background(
                    colorResource(id = R.color.popup_color), shape = RoundedCornerShape(corners)
                )
        ) {
            ConstraintLayout(
                Modifier
                    .heightIn(min = maxHeight)
                    .fillMaxWidth()
                    .padding(vertical = 16.dp)
            ) {
                val (titleView, contentView, buttonsContainer, progress) = createRefs()
                Text(text = title,
                    style = MaterialTheme.typography.titleLarge,
                    modifier = Modifier
                        .padding(horizontal = 16.dp)
                        .constrainAs(titleView) {
                            width = Dimension.fillToConstraints
                            height = Dimension.wrapContent
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo((parent.top))
                        })
                Box(modifier = Modifier
                    .padding(paddingValues = contentPaddingValues)
                    .constrainAs(contentView) {
                        width = Dimension.fillToConstraints
                        height = Dimension.preferredWrapContent
                        top.linkTo(titleView.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        bottom.linkTo(buttonsContainer.top)
                    }) {
                    content.invoke()
                }
                Row(horizontalArrangement = Arrangement.End,
                    modifier = Modifier.constrainAs(buttonsContainer) {
                        width = Dimension.fillToConstraints
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }) {

                    buttonList.forEach {
                        PopupButton(
                            text = it.text,
                            isEnabled = it.isEnabled,
                            onClick = it.onClick,
                            color =
                            if (it.isEnabled) MaterialTheme.colorScheme.secondary
                            else MaterialTheme.colorScheme.onBackground,
                        )
                    }
                    PopupButton(
                        text = stringResource(id = dismissButtonRes),
                        isEnabled = isLoading.not(),
                        onClick = onDismiss,
                        color = colorResource(
                            R.color.white
                        ),
                    )
                }
                if (isLoading) {
                    RetroProgressIndicator(
                        Modifier
                            .size(50.dp)
                            .constrainAs(progress) {
                                centerTo(parent)
                            })
                }
            }
        }
    })
}

class PopupButtonData(
    val text: String,
    val isEnabled: Boolean = true,
    val onClick: () -> Unit
)

@Composable
fun PopupButton(
    text: String, isEnabled: Boolean, color: Color, onClick: () -> Unit
) {
    Box(
        Modifier
            .alpha(if (isEnabled) 1f else 0.5f)
            .clickable(isEnabled, onClick = onClick)
    ) {
        Text(
            text = text,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp),
            style = TextStyle(
                color = color, fontSize = 16.sp, fontWeight = FontWeight.W600
            ),
        )
    }
}

private val corners = 4.dp
